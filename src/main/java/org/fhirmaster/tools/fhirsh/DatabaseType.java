package org.fhirmaster.tools.fhirsh;

/**
 *
 */
public enum DatabaseType {
    mssql,
    mysql,
    postgre
}
