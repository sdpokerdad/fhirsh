package org.fhirmaster.tools.fhirsh;

import javax.validation.Valid;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

/**
 *
 */
@ShellComponent
public class MyCommands {
    
    @ShellMethod("Add two integers")
    public int add(int a, int b) {
        return a+b;
    }
    
    @ShellMethod("Database migration scripts")
    public String migrate(@ShellOption(value = {"--db","--database"},help = "The type of database: mssql, mysql, or postgre") DatabaseType database) {
        return "db="+database;
    }
}
